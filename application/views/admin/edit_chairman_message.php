<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            <?php echo $title ?>
        </h1>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form role="form" action="<?php echo base_url(); ?>administrator/update_chairman_message" method="post" id="form" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Name</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="welcome_title" name="chairman_name" required value="<?php echo $chairman_info->chairman_name; ?>" ><br>
                                    <input type="hidden" class="form-control" id="welcome_id" name="chairman_id" required value="<?php echo $chairman_info->chairman_id; ?>" >

                                </div>     
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Short Message</label>
                                <div class="col-lg-9">
                                    <textarea class="form-control" maxlength="200" rows="2" id="welcome_description" name="chairman_short_message" required><?php echo $chairman_info->chairman_short_message; ?></textarea><br>
                                </div>     
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Long Message</label>
                                <div class="col-lg-9">
                                    <textarea class="form-control" rows="14" id="welcome_description" name="chairman_long_message" required><?php echo $chairman_info->chairman_long_message; ?></textarea><br>
                                </div>     
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Previous Image</label>
                                <div class="col-lg-9">
                                    <img src="<?php echo base_url().$chairman_info->image; ?>" width="200" height="200" ><br><br>
                                </div>     
                            </div>
                             
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Select Image</label>
                                <div class="col-lg-9">
                                <input type="file" name="image"><br>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label"></label>
                                <div class="col-lg-9">
                                    <button type="submit" id="submit" class="btn btn-primary">Update Changes</button>
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div>     
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
