<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">


    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Admin Panel</title>
        <!-- Bootstrap Styles-->
        <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FontAwesome Styles-->
        <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet" />
        <!-- Morris Chart Styles-->
        <link href="<?php echo base_url();?>assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- Custom Styles-->
        <link href="<?php echo base_url();?>assets/css/custom-styles.css" rel="stylesheet" />
        <!-- Google Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        
        <script type="text/javascript">
        
        function check_delete()
        {
           var chk = confirm('Are you sure to delete this!');
                if (chk)
                {
                    return true;
                }
                else
                {
                    return false;
                }
        }
        </script>
    </head>

    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default top-navbar" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index-2.html">Admin Panel</a>
                </div>

                <ul class="nav navbar-top-links navbar-right">



                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                            <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i> Admin Name
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url();?>administrator/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
            </nav>
            <!--/. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="main-menu">

                        <li>
                            <a href="<?php echo base_url();?>administrator/index"><i class="fa fa-file"></i> Dashboard</a>
                        </li>
                        
                        <li>
                            <a  href="<?php echo base_url();?>administrator/welcome_message"><i class="fa fa-edit"></i> Welcome Message</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>administrator/chairman_message"><i class="fa fa-edit"></i>Chairman Message</i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-edit"></i>Board Member<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url();?>administrator/add_board_member">Add Board Member</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>administrator/view_board_member">View Board Member</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>administrator/advisory"><i class="fa fa-edit"></i> Advisory Committee</a>
                        </li>
                         <li>
                            <a href="<?php echo base_url();?>administrator/service"><i class="fa fa-edit"></i> Services</a>
                        </li>
                        
                         <li>
                            <a href="<?php echo base_url();?>administrator/glance"><i class="fa fa-edit"></i> At a Glance</a>
                        </li>
                        
                         <li>
                            <a href="<?php echo base_url();?>administrator/work_force"><i class="fa fa-edit"></i>Work Force</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>administrator/gallery"><i class="fa fa-edit"></i>Photo Gallery</a>
                        </li>
                        
                    </ul>

                </div>

            </nav>
            <!-- /. NAV SIDE  -->
            <div id="page-wrapper">
                <div id="page-inner">
                    
                    
                    <?php echo $admin_mid_content;?>
                    
                    
                    

                </div>
            </div>
        </div>

        </div>
        </div>
        <!-- /. ROW  -->
        
        <footer>
            <div class="row">
                <div class="col-lg-3">
                    <p><a href="www.dynamicsoftltd.com">Dynamic software Ltd</a></p>
                </div>
                 <div class="col-lg-3">
                     
                </div>
                 <div class="col-lg-3">
                    <p></p>
                </div>
                <div class="col-lg-3">
                    <p>Template by: <a href="">Mustafizur Rahman</a></p>
                </div>
            </div>
            
        </footer>
        
        </div>
        <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
        </div>
        <!-- /. WRAPPER  -->
        <!-- JS Scripts-->
        <!-- jQuery Js -->
        <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.js"></script>
        <!-- Bootstrap Js -->
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
        <!-- Metis Menu Js -->
        <script src="<?php echo base_url();?>assets/js/jquery.metisMenu.js"></script>
        <!-- Morris Chart Js -->
        <script src="<?php echo base_url();?>assets/js/morris/raphael-2.1.0.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/morris/morris.js"></script>
        <!-- Custom Js -->
        <script src="<?php echo base_url();?>assets/js/custom-scripts.js"></script>
        
<!--         <script src="<?php echo base_url();?>assets/js/jquery-1.11.2.js"></script>-->


    </body>


    
</html>