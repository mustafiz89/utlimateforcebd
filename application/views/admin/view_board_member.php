<style>
    .p{
        margin: -5px;
        padding: 0px;
       
        font-size: 12px;
    }
    
</style>

<div class="row">
    <div class="col-md-12 col-lg-12">
        <!--   Kitchen Sink -->
        <div class="panel panel-default">
            <div class="panel-heading">
              <h1 style="color:#898989">
            <?php echo $title ?>
            <p style="color: green;" id="message">
                <?php 
                $msg=$this->session->userdata('message');
                if($msg){
                    echo $msg;
                    $this->session->unset_userdata('message');
                }               
                ?>
            </p>
        </h1>        
             </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Serial</th>
                                <th>Details Information</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=0;
                            foreach($all_info as $v_info)
                            {
                                $i+=1;
                            ?>                        
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <p class='p'>Name : <?php echo $v_info->member_name;?></p>
                                    <p class='p'>Designation : <?php echo $v_info->designation?></p> 
                                    <p class='p'>Father's Name : <?php echo $v_info->f_name;?></p> 
                                    <p class='p'>Mother's Name : <?php echo $v_info->m_name; ?></p> 
                                    <p class='p'>Home Town : <?php echo $v_info->home_town;?></p>
                                    <p class='p'>Living At (Settled) : <?php echo $v_info->living_at;?></p> 
                                    <p class='p'>Educational Qualification : <?php echo $v_info->educational_qualification;?></p>
                                    <p class='p'>Business Portfolio : <?php echo $v_info->business_portfolio;?></p>
                                </td>
                                <td><img src="<?php echo base_url().$v_info->image?>" width="100" height="100"></td>
                                <td>
                                    <a class="btn btn-primary" href="<?php echo base_url();?>administrator/edit_board_member/<?php echo $v_info->member_id;?>">
                                    <i class="fa fa-edit"></i>  
                                    Edit                                            
                                </a>
                                    <a class="btn btn-danger" href="<?php echo base_url();?>administrator/delete_board_member/<?php echo $v_info->member_id;?>" onclick="return check_delete();">
                                    <i class="fa fa-trash-o"></i>  
                                    Delete                                        
                                </a>
                                </td>
                            </tr>
                            <?php 
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
    
</div>

