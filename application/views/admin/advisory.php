
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 style="color:#898989">
                    <?php echo $title ?>
                    <p style="color: green;" id="message">
                        <?php
                        $msg = $this->session->userdata('message');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('message');
                        }
                        ?>
                    </p>
                </h1>        
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form role="form" action="<?php echo base_url(); ?>administrator/update_advisory_info" method="post" id="form" >
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Name</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="welcome_title" name="title" required value="<?php echo $all_info->title; ?>" ><br>
                                    <input type="hidden" class="form-control" id="welcome_id" name="id" required value="<?php echo $all_info->id; ?>" >

                                </div>     
                            </div>



                            <div class="form-group">
                                <label class="col-lg-3 control-label">Description</label>
                                <div class="col-lg-9">
                                    <textarea class="form-control" rows="20" id="welcome_description" name="description" required><?php echo $all_info->description; ?></textarea><br>
                                </div>     
                            </div>



                            <div class="form-group">
                                <label class="col-lg-3 control-label"></label>
                                <div class="col-lg-9">
                                    <button type="submit" id="submit" class="btn btn-primary">Update Changes</button>
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div>     
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
