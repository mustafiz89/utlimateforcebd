<!DOCTYPE html>
<html lang="en">

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>

        <meta charset="utf-8">
        <title>Admin Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Admin template.">
        <meta name="author" content="Mustafizur Rahman">

        <link id="bs-css" href="<?php echo base_url();?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/css/charisma-app.css" rel="stylesheet">
        <link rel="shortcut icon" href="img/favicon.ico">


    </head>
    <body>
        <div class="ch-container">
            <div class="row">
                <div class="row">
                    <div class="col-md-12 center login-header">
                        <h2>Welcome to Admin Panel</h2>
                    </div>
                    

                </div> 
                
                <div class="row">
                    <div class="well col-md-5 center login-box">
                        <div class="alert alert-info">

                            <?php
                            $msg = $this->session->userdata('message');
                            if ($msg) {
                                
                                echo '<p style="color: green;">';
                                echo $msg;
                                echo '</p>';
                                $this->session->unset_userdata('message');
                            } 
                            
                            
                            
                            ?>

                            <?php
                            $msg = $this->session->userdata('exception');
                            if ($msg) {
                                
                                echo '<p style="color: red;">';
                                echo $msg;
                                echo '</p>';
                                $this->session->unset_userdata('exception');
                            } 
                            else {
                                echo 'Please login with your Username and Password.';
                            }
                            
                            ?>

                        </div>
                        <form class="form-horizontal" action="<?php echo base_url();?>login/admin_login_check" method="post">
                            <fieldset>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="icon-home"></i></span>
                                    
                                    <input type="text" class="form-control" placeholder="Username" name="admin_name" value="">
                                    
                                </div>
                                <div class="clearfix"></div><br>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"></span>
                                    
                                    <input type="password" class="form-control" placeholder="Password" name="admin_password" value="">
                                    
                                </div>
                                <div class="clearfix"></div>
                                <div class="input-prepend">
                                    
                                    <label class="remember" for="remember"><input type="checkbox" id="remember"> Remember me</label>
                                    
                                </div>
                                <div class="clearfix"></div>
                                <p class="center col-md-5">
                                    
                                    <button type="submit" class="btn btn-primary">Login</button>
                                    
                                    
                                </p>
                            </fieldset>
                        </form>
                    </div>

                </div> 
            </div> 
        </div> 
    </body>

</html>
