<!DOCTYPE html ">
<html xmlns="http://www.w3.org/1999/xhtml">

<head >
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Ultimate Force BD</title>
<link rel="shortcut icon" href="images/favicon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="imagetoolbar" content="no" />
<link rel="stylesheet" href="<?php echo base_url();?>styles/layout.css" type="text/css" />
<style type="text/css">
#osfooter{display:block;position:fixed;bottom:0;left:0;width:100%;height:300px;margin-bottom:-300px;overflow:hidden;background-color:transparent;z-index:5000;text-indent:-5000px;}
#osfooter div{margin-bottom:-1000px;}
#osfooter a{display:block; text-indent:-5000px;}
</style>
<!--[if lte IE 6]><style type="text/css">#osfooter{position:absolute; display:none;}</style><![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/nivo-slider.css" />
  <script type="text/javascript" src="<?php echo base_url();?>js/jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>js/jquery.nivo.slider.pack.js"></script>
  <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
  </script> 

</head>
<body id="top">

<div class="wrapper col1">
  <div id="header">
    <div id="logo">
<!--        <h1><a href="<?php echo base_url();?>welcome/index">Ultimate Force BD</a></h1>-->
<!--      <p><strong>Ensure your Security</strong></p>-->
    </div>
    
    <br class="clear" />
  </div>
</div>
<div class="wrapper col2">
  <div id="topbar">
    <div id="topnav">
      <ul>
        <li ><a href="<?php echo base_url();?>welcome/index">Home</a></li>
        <li><a href="#">Board Of Director</a>
          <ul>
            <li><a href="<?php echo base_url();?>welcome/chairman_message">Chairman Message</a></li>
            <li><a href="<?php echo base_url();?>welcome/board_member">Board Member</a></li>
            
          </ul>
        </li>
        <li><a href="#">Who We are</a>
          <ul>
            <li><a href="<?php echo base_url();?>welcome/glance">At a Glance</a></li>
            <li><a href="<?php echo base_url();?>welcome/work_force">Work Force</a></li>
            
          </ul>
        </li>
        <li ><a href="<?php echo base_url();?>welcome/service">Our Services</a></li>
         <li ><a href="<?php echo base_url();?>welcome/advisory_committee">Advisory Committee</a></li>
          <li ><a href="<?php echo base_url();?>welcome/gallery">Gallery</a></li>
          <li ><a href="<?php echo base_url();?>welcome/contact">Contact</a></li>
      </ul>
    </div>
    
    <br class="clear" />
  </div>
</div>
<div class="wrapper col5">
  
    <?php echo $mid_content;?>
</div>


<div class="wrapper col6">
<div id="footer">
<div class="footbox">
      <h2>Contact With Us</h2>
      <ul>
        <li><a>House:</a></li>
        <li><a>Road:</a></li>
        <li><a></a></li>
        <li><a></a></li>
        <li><a>Dhaka, Bangladesh</a></li>
      </ul>
    </div>
    <br class="clear" />
</div>
      
</div>











<div class="wrapper col7">
  <div id="copyright">
    <p class="fl_left">Copyright &copy; 2015 - All Rights Reserved - <a href="">Ultimateforcebd.com</a></p>
    <p class="fl_right">Develop By <a href="" title="">Dynamic Software Ltd</a></p>
    <br class="clear" />
  </div>
</div>

</body>

</html>
