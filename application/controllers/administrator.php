<?php 
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Administrator extends CI_Controller {    
    
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==null)
        {
            redirect('login','refresh');
        }
    }

        public function logout()
        {
            $this->session->unset_userdata('admin_id');
            $this->session->unset_userdata('admin_name');
            $sdata=array();
            $sdata['message']='You are successfully Logout';
            $this->session->set_userdata($sdata);
            redirect('login','refresh');
            
        }
	
	public function index()
	{
            $data=array();
            $data['title']='Dashboard'; 
            $data['admin_mid_content']=$this->load->view('admin/dashboard',$data,true);
            $this->load->view('admin/admin_master',$data);
           
	}
        
        public function welcome_message()
        {
            $data=array();
            $data['title']='Welcome Message';
            $data['welcome_info']=$this->administrator_model->select_welcome_info_by_id(1);
            
            
            
            $data['admin_mid_content']=$this->load->view('admin/welcome_message',$data,true);
            $this->load->view('admin/admin_master',$data);
        }
        
        public function update_welcome_info()
        {
            $data=array();
            $welcome_id=$this->input->post('welcome_id',true);
            $data['welcome_title']=$this->input->post('welcome_title',true);
            $data['welcome_description']=$this->input->post('welcome_description',true);
            
//            echo '<pre>';
//            print_r($data);
//            exit;
            
//            $welcome_id=$this->input->post('welcome_id',true);
//            $data['welcome_title']=$this->input->post('welcome_title',true);
//            $data['welcome_description']=$this->input->post('welcome_description',true); 
            
            $this->administrator_model->update_welcome_info($welcome_id,$data);
            
            $sdata=array();
            $sdata['message']='Update Successfully';
            $this->session->set_userdata($sdata);
            redirect('administrator/welcome_message');
        }
        
    /**/
    /**/
    /**//**/
    /**/
    /**/   
        public function chairman_message()
        {
            $data=array();
            $data['title']='Chairman Message';
            $data['chairman_info']=$this->administrator_model->select_chairman_message_by_id(1);
            $data['admin_mid_content']=$this->load->view('admin/chairman_message',$data,true);
            $this->load->view('admin/admin_master',$data);
        }
        
        public function chairman_message_edit($chairman_id){
            $data=array();
            $data['title']='Edit Information';
            $data['chairman_info']=$this->administrator_model->select_chairman_message_by_id($chairman_id);            
            $data['admin_mid_content']=$this->load->view('admin/edit_chairman_message',$data,true);
            $this->load->view('admin/admin_master',$data);
        }
        
        public function update_chairman_message()
        {
        $data = array();
        $chairman_id = $this->input->post('chairman_id', true);
        $data['chairman_name'] = $this->input->post('chairman_name', true);
        $data['chairman_short_message'] = $this->input->post('chairman_short_message', true);
        $data['chairman_long_message'] = $this->input->post('chairman_long_message', true);
        $db_image = $this->administrator_model->select_chairman_message_by_id($chairman_id);
        
        /* upload Image */

        $config['upload_path'] = 'images/chairman/';
        $config['allowed_types'] = 'jpeg|jpg|gif|png';
        $config['max_size'] = '3000';
        $config['max_width'] = '2000';
        $config['max_height'] = '1200';
        $error = '';
        $fdata = array();

        $this->load->library('upload', $config);
        
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            echo $error;
        } 
        else 
        {
            
            $fdata = $this->upload->data();

            if ($fdata['file_name'] == '') {                
                return;
            } 
            else {

                if ($db_image->image) {
                    $image = $db_image->image;
                    unlink($image);
                }
                $data['image'] = $config['upload_path'] . $fdata['file_name'];
            }
        }
        
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        $this->administrator_model->update_chairman_message($chairman_id,$data);
        $sdata=array();
        $sdata['message']="Update Successfully";
        $this->session->set_userdata($sdata);
        redirect('administrator/chairman_message');
    }
    
    /**/
    /**/
    /**//**/
    /**/
    /**/   
    
    public function add_board_member()
    {
        $data=array();
        $data['title']='Add Board Member';
        $data['admin_mid_content']=$this->load->view('admin/add_board_member',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_board_member()
    {
        $data=array();
        $data['member_name']=$this->input->post('member_name',true);
        $data['designation']=$this->input->post('designation',true);
        $data['f_name']=$this->input->post('f_name',true);
        $data['m_name']=$this->input->post('m_name',true);
        $data['home_town']=$this->input->post('home_town',true);
        $data['living_at']=$this->input->post('living_at',true);
        $data['educational_qualification']=$this->input->post('educational_qualification',true);
        $data['business_portfolio']=$this->input->post('business_portfolio',true);
//        echo '<pre>';
//        print_r($data);
//        exit;
        
        /*upload image*/
        $config['upload_path'] = 'images/board_member/';
        $config['allowed_types'] = 'jpeg|jpg|gif|png';
        $config['max_size'] = '3000';
        $config['max_width'] = '2000';
        $config['max_height'] = '1200';
        $error = '';
        $fdata = array();

        $this->load->library('upload', $config);
        
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            echo $error;
        } 
        else 
        {
            
            $fdata = $this->upload->data();
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
            
        }
//     echo '<pre>';
//     print_r($data);
//     exit;
       $this->administrator_model->save_board_member_info($data);
       $sdata=array();
       $sdata['message']='Save Successfully';
       $this->session->set_userdata($sdata);
       redirect('administrator/add_board_member');
    }
    
    public function view_board_member()
    {
        $data=array();
        $data['title']='Board Member';
        $data['all_info']=$this->administrator_model->select_all_board_member_data();
        $data['admin_mid_content']=$this->load->view('admin/view_board_member',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function edit_board_member($member_id)
    {
        $data=array();
        $data['title']='Edit Information';
        $data['edit_info']=$this->administrator_model->select_board_member_by_id($member_id);
        $data['admin_mid_content']=$this->load->view('admin/edit_board_member',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
     
    public function update_board_member()
    {
        $data=array();
        $member_id=$this->input->post('member_id',true);
        $data['member_name']=$this->input->post('member_name',true);
        $data['designation']=$this->input->post('designation',true);
        $data['f_name']=$this->input->post('f_name',true);
        $data['m_name']=$this->input->post('m_name',true);
        $data['home_town']=$this->input->post('home_town',true);
        $data['living_at']=$this->input->post('living_at',true);
        $data['educational_qualification']=$this->input->post('educational_qualification',true);
        $data['business_portfolio']=$this->input->post('business_portfolio',true);
        $db_image=$this->administrator_model->select_board_member_by_id($member_id);
        
         /* upload Image */

        $config['upload_path'] = 'images/board_member/';
        $config['allowed_types'] = 'jpeg|jpg|gif|png';
        $config['max_size'] = '3000';
        $config['max_width'] = '2000';
        $config['max_height'] = '1200';
        $error = '';
        $fdata = array();

        $this->load->library('upload', $config);
        
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            echo $error;
        } 
        else 
        {
            
            $fdata = $this->upload->data();

            if ($fdata['file_name'] == '') {                
                return;
            } 
            else {

                if ($db_image->image) {
                    $image = $db_image->image;
                    unlink($image);
                }
                $data['image'] = $config['upload_path'] . $fdata['file_name'];
            }
        }
        
        $this->administrator_model->update_board_member_by_id($member_id,$data);
        $sdata=array();
        $sdata['message']='Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/view_board_member');
        
        
    }
    public function delete_board_member($member_id)
    {
        $db_image=$this->administrator_model->select_board_member_by_id($member_id);
         if ($db_image->image) {
            $image = $db_image->image;
            unlink($image);
        }
        $this->administrator_model->delete_board_member_by_id($member_id); 
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/view_board_member');
    }
    /**/
    /**/
    /**//**/
    /**/
    /**/   
    
    public function advisory()
    {
        $data=array();
        $data['title']='Advisory Committee';
        $data['all_info']=$this->administrator_model->select_advisory_committee_by_id(1);
        $data['admin_mid_content']=$this->load->view('admin/advisory',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    
    public function update_advisory_info()
    {
        $data=array();
        $id=$this->input->post('id',true);
        $data['title']=$this->input->post('title',true);
        $data['description']=$this->input->post('description',true);
        $this->administrator_model->update_advisory_committee_by_id($id,$data);
        $sdata=array();
        $sdata['message']='Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/advisory');
    }
    /**/
    /**/
    /**//**/
    /**/
    /**/
    public function service()
    {
        $data=array();
        $data['title']='Advisory Committee';
        $data['all_info']=$this->administrator_model->select_service_by_id(1);
        $data['admin_mid_content']=$this->load->view('admin/service',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function update_service_info()
    {
        $data=array();
        $id=$this->input->post('id',true);
        $data['title']=$this->input->post('title',true);
        $data['description']=$this->input->post('description',true);
        $this->administrator_model->update_service_by_id($id,$data);
        $sdata=array();
        $sdata['message']='Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/service');
    }
    /**/
    /**/
    /**//**/
    /**/
    /**/
    
    public function glance()
    {
        $data=array();
        $data['title']='At a Glance';
        $data['all_info']=$this->administrator_model->select_glance_by_id(1);
        $data['admin_mid_content']=$this->load->view('admin/glance',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function update_glance_info()
    {
        $data=array();
        $id=$this->input->post('id',true);
        $data['title']=$this->input->post('title',true);
        $data['description']=$this->input->post('description',true);
        $this->administrator_model->update_glance_by_id($id,$data);
        $sdata=array();
        $sdata['message']='Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/glance');
    }
    
    /**/
    /**/
    /**//**/
    /**/
    /**/
    
    public function work_force()
    {
        $data=array();
        $data['title']='Work Force';
        $data['all_info']=$this->administrator_model->select_work_force_by_id(1);
        $data['admin_mid_content']=$this->load->view('admin/work_force',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function update_work_force_info()
    {
        $data=array();
        $id=$this->input->post('id',true);
        $data['title']=$this->input->post('title',true);
        $data['description']=$this->input->post('description',true);
        $this->administrator_model->update_work_force_by_id($id,$data);
        $sdata=array();
        $sdata['message']='Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/glance');
    }
    /**/
    /**/
    /**//**/
    /**/
    /**/
    public function gallery()
    {
        $data=array();
        $data['title']='Photo Gallary';
        $data['all_info']=$this->administrator_model->select_all_gallery();
        $data['admin_mid_content']=$this->load->view('admin/gallery',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function save_gallery()
    {
        $data=array();
         /*upload image*/
        $config['upload_path'] = 'images/gallery/';
        $config['allowed_types'] = 'jpeg|jpg|gif|png';
        $config['max_size'] = '3000';
        $config['max_width'] = '2000';
        $config['max_height'] = '1200';
        $error = '';
        $fdata = array();

        $this->load->library('upload', $config);
        
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            echo $error;
        } 
        else 
        {
            
            $fdata = $this->upload->data();
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
            
        }
//     echo '<pre>';
//     print_r($data);
//     exit;
       $this->administrator_model->save_gallery_info($data);
       $sdata=array();
       $sdata['message1']='Save Successfully';
       $this->session->set_userdata($sdata);
       redirect('administrator/gallery');
    }
    public function delete_gallery($id)
    {
        $db_image=$this->administrator_model->select_gallery_by_id($id);
         if ($db_image->image) {
            $image = $db_image->image;
            unlink($image);
        }
        $this->administrator_model->delete_gallery_by_id($id); 
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/gallery');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>