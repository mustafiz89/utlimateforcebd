<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
            $data=array();
            $data['title']='Ultimate Force BD';
            $data['all_info']=$this->welcome_model->select_welcome_by_id(1);
            $data['mid_content']=$this->load->view('home_content',$data,true);
            $this->load->view('master',$data);
	}
        public function chairman_message()
        {
            $data=array();
            $data['title']='Chairman Message';
            $data['all_info']=$this->welcome_model->select_chairman_by_id(1);
            $data['mid_content']=$this->load->view('chairman_message',$data,true);
            $this->load->view('master',$data);
        }
        public function board_member()
        {
            $data=array();
            $data['title']='Board Member';
            $data['all_info']=$this->welcome_model->select_all_member();
            $data['mid_content']=$this->load->view('board_member',$data,true);
            $this->load->view('master',$data);
        }
        
        public function service()
        {
            $data=array();
            $data['title']='Our Service';
            $data['all_info']=$this->welcome_model->select_service_by_id(1);
            $data['mid_content']=$this->load->view('service',$data,true);
            $this->load->view('master',$data);
        }
        
        public function glance()
        {
            $data=array();
            $data['title']='At a Glance';
            $data['all_info']=$this->welcome_model->select_glance_by_id(1);
            $data['mid_content']=$this->load->view('glance',$data,true);
            $this->load->view('master',$data);
        }
        public function gallery()
        {
            $data=array();
            $data['title']='Photo Gallery';
            $data['all_info']=$this->welcome_model->select_all_gallery();
            $data['mid_content']=$this->load->view('gallery',$data,true);
            $this->load->view('master',$data);
        }
        public function work_force()
        {
            $data=array();
            $data['title']='Work Force';
            $data['all_info']=$this->welcome_model->select_work_force_by_id(1);
            $data['mid_content']=$this->load->view('work_force',$data,true);
            $this->load->view('master',$data);
        }
        public function advisory_committee()
        {
            $data=array();
            $data['title']='Advisory Committee';
            $data['all_info']=$this->welcome_model->select_advisory_by_id(1);
            $data['mid_content']=$this->load->view('advisory_committee',$data,true);
            $this->load->view('master',$data);
        }
        
        public function contact()
        {
            $data=array();
            $data['title']='Send Us an Email';            
            $data['mid_content']=$this->load->view('contact',$data,true);
            $this->load->view('master',$data);
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */