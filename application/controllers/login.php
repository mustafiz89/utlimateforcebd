<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();

class Login extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id!=null)
        {
            redirect('administrator','refresh');
        }
    }

	public function index()
	{
		$this->load->view('admin/admin_login');
	}
        
        public function admin_login_check()
        {
            
            $admin_name=$this->input->post('admin_name',true);
            $admin_password=$this->input->post('admin_password',true);
            $result=$this->login_model->select_admin_by_data($admin_name,$admin_password);
            $sdata=array();
            if($result)
            {               
                $sdata['admin_id']=$result->admin_id;
                $sdata['admin_name']=$result->admin_name;
                $this->session->set_userdata($sdata);
                redirect('administrator','refresh');
            }
            else
            {
                $sdata['exception']='Invalid Username or Password';
                $this->session->set_userdata($sdata);
                redirect('login');
            }
        }   
}
?>