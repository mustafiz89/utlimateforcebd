<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Administrator_model extends CI_Model{
    
//    public function select_subcategory_by_id($category_id)
//    {
//        $this->db->select('*');
//        $this->db->from('tbl_subcategory');
//        $this->db->join('tbl_category','tbl_category.category_id=tbl_subcategory.category_id');
//        $this->db->where('tbl_category.category_id',$category_id);
//        $query_result=$this->db->get();
//        $result=$query_result->result();
//        return $result;
//    }
//    public function select_all_category()
//    {
//        $this->db->select('*');
//        $this->db->from('tbl_category');
//        $query_result=$this->db->get();
//        $result=$query_result->result();
//        return $result;
//    }
    
    public function select_welcome_info_by_id($welcome_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_welcome_message');
        $this->db->where('welcome_id',$welcome_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function update_welcome_info($welcome_id,$data)
    {
        $this->db->where('welcome_id',$welcome_id);
        $this->db->update('tbl_welcome_message',$data);
       
    }
    
    public function select_chairman_message_by_id($chairman_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_chairman_message');
        $this->db->where('chairman_id',$chairman_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function update_chairman_message($chairman_id,$data)
    {
        $this->db->where('chairman_id',$chairman_id);
        $this->db->update('tbl_chairman_message',$data);
    }
    
    public function save_board_member_info($data)
    {
        $this->db->insert('tbl_board_member',$data);
    }
    
    public function select_all_board_member_data()
    {
        $this->db->select('*');
        $this->db->from('tbl_board_member');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function select_board_member_by_id($member_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_board_member');
        $this->db->where('member_id',$member_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function update_board_member_by_id($member_id, $data)
    {
        $this->db->where('member_id',$member_id);
        $this->db->update('tbl_board_member',$data);
    }
    public function delete_board_member_by_id($member_id)
    {
        $this->db->where('member_id',$member_id);
        $this->db->delete('tbl_board_member');
    }
    
    /**/
    /**/
    /**//**/
    /**/
    /**/
    
    public function select_advisory_committee_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_advisory_committee');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function update_advisory_committee_by_id($id, $data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_advisory_committee',$data);
    }
    /**/
    /**/
    /**//**/
    /**/
    /**/
    
     public function select_service_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_service');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function update_service_by_id($id, $data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_service',$data);
    }
    /**/
    /**/
    /**//**/
    /**/
    /**/
    
    public function select_glance_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_glance');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function update_glance_by_id($id, $data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_glance',$data);
    }
    /**/
    /**/
    /**//**/
    /**/
    /**/
    
    public function select_work_force_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_work_force');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function update_work_force_by_id($id, $data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_work_force',$data);
    }
     /**/
    /**/
    /**//**/
    /**/
    /**/
    public function select_all_gallery()
    {
        $this->db->select('*');
        $this->db->from('tbl_gallery');
        //$this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function save_gallery_info($data)
    {
        $this->db->insert('tbl_gallery',$data);
    }
    public function select_gallery_by_id($id)
    {
         $this->db->select('*');
        $this->db->from('tbl_gallery');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function delete_gallery_by_id($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_gallery');
    }
}
?>