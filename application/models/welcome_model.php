<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Welcome_model extends CI_Model{
    
    
    public function select_welcome_by_id($welcome_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_welcome_message');
        $this->db->where('welcome_id',$welcome_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;        
    }
    public function select_chairman_by_id($chairman_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_chairman_message');
        $this->db->where('chairman_id',$chairman_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function select_all_member()
    {
        $this->db->select('*');
        $this->db->from('tbl_board_member');
        
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
     public function select_service_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_service');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function select_glance_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_glance');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function select_all_gallery()
    {
         $this->db->select('*');
        $this->db->from('tbl_gallery');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function select_work_force_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_work_force');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
     public function select_advisory_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_advisory_committee');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    
    
}