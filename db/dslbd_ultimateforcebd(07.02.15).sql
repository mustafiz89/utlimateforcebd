-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2015 at 12:48 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dslbd_ultimateforcebd`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
`admin_id` int(3) NOT NULL,
  `admin_name` varchar(50) DEFAULT NULL,
  `admin_email` varchar(100) DEFAULT NULL,
  `admin_password` varchar(32) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`) VALUES
(1, 'admin', 'admin@admin.com', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_advisory_committee`
--

CREATE TABLE IF NOT EXISTS `tbl_advisory_committee` (
`id` int(4) NOT NULL,
  `title` varchar(80) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_advisory_committee`
--

INSERT INTO `tbl_advisory_committee` (`id`, `title`, `description`) VALUES
(1, 'title', 'description');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_board_member`
--

CREATE TABLE IF NOT EXISTS `tbl_board_member` (
`member_id` int(3) NOT NULL,
  `member_name` varchar(50) DEFAULT NULL,
  `designation` varchar(80) DEFAULT NULL,
  `f_name` varchar(50) DEFAULT NULL,
  `m_name` varchar(50) DEFAULT NULL,
  `home_town` varchar(50) DEFAULT NULL,
  `living_at` varchar(50) DEFAULT NULL,
  `educational_qualification` varchar(80) DEFAULT NULL,
  `business_portfolio` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_board_member`
--

INSERT INTO `tbl_board_member` (`member_id`, `member_name`, `designation`, `f_name`, `m_name`, `home_town`, `living_at`, `educational_qualification`, `business_portfolio`, `image`) VALUES
(1, 'ab', 'aa', 'ab', 'ab', 'ab', 'ab', 'ab', 'ab', 'images/board_member/user1.jpg'),
(3, 'bb', 'bb', 'bb', 'bb', 'bb', 'bb', 'bb', 'bb', 'images/board_member/user.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chairman_message`
--

CREATE TABLE IF NOT EXISTS `tbl_chairman_message` (
`chairman_id` int(3) NOT NULL,
  `chairman_name` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `chairman_short_message` text CHARACTER SET latin1,
  `chairman_long_message` text CHARACTER SET latin1,
  `image` varchar(50) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_chairman_message`
--

INSERT INTO `tbl_chairman_message` (`chairman_id`, `chairman_name`, `chairman_short_message`, `chairman_long_message`, `image`) VALUES
(1, 'Chairman Name', 'chairman short message', 'chairman Long message chairman Long message chairman Long message chairman Long message chairman Long message chairman Long message chairman Long message  chairman Long message chairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long messagechairman Long message', 'images/chairman/facebook-avatar.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE IF NOT EXISTS `tbl_gallery` (
`id` int(11) NOT NULL,
  `image` varchar(80) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`id`, `image`) VALUES
(1, 'images/gallery/facebook-avatar1.png'),
(2, 'images/gallery/user1.jpg'),
(3, 'images/gallery/facebook-avatar2.png'),
(4, 'images/gallery/user2.jpg'),
(5, 'images/gallery/facebook-avatar3.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_glance`
--

CREATE TABLE IF NOT EXISTS `tbl_glance` (
`id` int(3) NOT NULL,
  `title` varchar(80) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_glance`
--

INSERT INTO `tbl_glance` (`id`, `title`, `description`) VALUES
(1, 'title', 'description');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service`
--

CREATE TABLE IF NOT EXISTS `tbl_service` (
`id` int(4) NOT NULL,
  `title` varchar(80) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_service`
--

INSERT INTO `tbl_service` (`id`, `title`, `description`) VALUES
(1, 'title', '1. description\r\n\r\n2.description 2 \r\n\r\n3. description 3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_welcome_message`
--

CREATE TABLE IF NOT EXISTS `tbl_welcome_message` (
`welcome_id` int(3) NOT NULL,
  `welcome_title` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `welcome_description` text CHARACTER SET latin1
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_welcome_message`
--

INSERT INTO `tbl_welcome_message` (`welcome_id`, `welcome_title`, `welcome_description`) VALUES
(1, 'welcome_title', 'welcome_description');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_force`
--

CREATE TABLE IF NOT EXISTS `tbl_work_force` (
`id` int(3) NOT NULL,
  `title` varchar(80) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_work_force`
--

INSERT INTO `tbl_work_force` (`id`, `title`, `description`) VALUES
(1, 'title', 'description');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
 ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_advisory_committee`
--
ALTER TABLE `tbl_advisory_committee`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_board_member`
--
ALTER TABLE `tbl_board_member`
 ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `tbl_chairman_message`
--
ALTER TABLE `tbl_chairman_message`
 ADD PRIMARY KEY (`chairman_id`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_glance`
--
ALTER TABLE `tbl_glance`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_service`
--
ALTER TABLE `tbl_service`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_welcome_message`
--
ALTER TABLE `tbl_welcome_message`
 ADD PRIMARY KEY (`welcome_id`);

--
-- Indexes for table `tbl_work_force`
--
ALTER TABLE `tbl_work_force`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
MODIFY `admin_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_advisory_committee`
--
ALTER TABLE `tbl_advisory_committee`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_board_member`
--
ALTER TABLE `tbl_board_member`
MODIFY `member_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_chairman_message`
--
ALTER TABLE `tbl_chairman_message`
MODIFY `chairman_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_glance`
--
ALTER TABLE `tbl_glance`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_service`
--
ALTER TABLE `tbl_service`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_welcome_message`
--
ALTER TABLE `tbl_welcome_message`
MODIFY `welcome_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_work_force`
--
ALTER TABLE `tbl_work_force`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
